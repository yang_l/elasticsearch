package com.example.elastic_search.config;


import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import co.elastic.clients.util.ContentType;
import org.apache.http.*;
import org.apache.http.message.BasicHeader;
import org.elasticsearch.client.RestClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;

/**
 * @Author YangLi
 * @Date 2023/10/26 9:52
 * @注释
 */
@Configuration
public class ElasticSearchConfig {

    private static final String IP = "localhost";
    private static final Integer PORT = 9200;

    @Bean
    public ElasticsearchClient restHighLevelClient() {
        RestClient restClient = RestClient.builder(new HttpHost(IP, PORT))
                // 设置header信息   使用lamda表达式 给需要的 item 加上header信息 setDefaultHeaders（）里面需要一个list 存放contentType和 value
                .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultHeaders(Collections.singletonList(new BasicHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON)))
                        // 设置响应体的header 使用lamda表达式addHeader
                        .addInterceptorLast((HttpResponseInterceptor) (response, context) -> response.addHeader("X-Elastic-Product", "Elasticsearch"))).build();

        ElasticsearchTransport transport = new RestClientTransport(restClient, new JacksonJsonpMapper());
        return new ElasticsearchClient(transport);
    }
}
