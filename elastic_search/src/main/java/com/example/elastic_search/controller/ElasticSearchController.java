package com.example.elastic_search.controller;

import com.example.elastic_search.Service.ElasticSearchService;
import com.example.elastic_search.domain.requestDTO.*;
import com.example.elastic_search.domain.entity.User;
import com.example.elastic_search.result.RestResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * @Author YangLi
 * @Date 2023/10/26 10:06
 * @注释
 */
@RestController
@RequestMapping(value = "/es")
public class ElasticSearchController {

    @Resource
    private ElasticSearchService elasticSearchService;

    // 创建索引
    @PutMapping("/createIndex")
    public RestResult<String> createIndex(String index) throws IOException {
        return elasticSearchService.createIndex(index);
    }

    // 删除索引
    @DeleteMapping("/deleteIndex")
    public RestResult<String> deleteIndex(String index) throws IOException {
        return elasticSearchService.deleteIndex(index);
    }

    // 创建/添加数据文档
    @PutMapping("/createDocument")
    public RestResult<String> createDocument(@RequestBody User user) {
        return elasticSearchService.createDocument(user);
    }

    // 批量创建/添加文档 （单个添加 list中填一个就行了）
    @PostMapping("/addDocument")
    public RestResult<String> addDocument(@RequestBody AddUserDTO addUserDTO) throws IOException {
        return elasticSearchService.addDocument(addUserDTO);
    }

    // 修改数据文档
    @PostMapping("/updateDocument")
    public RestResult<String> updateDocument(@RequestBody UpdateDocumentDTO updateDocumentDTO) throws IOException {
        return elasticSearchService.updateDocument(updateDocumentDTO);
    }

    // 删除索引中的某一条文档
    @DeleteMapping("/deleteDocumentByIndexAndId")
    public RestResult<String> deleteDocumentByIndexAndId(@RequestBody DeleteDocumentByIndexAndIdDTO deleteDocumentByIndexAndIdDTO) {
        return elasticSearchService.deleteDocumentByIndexAndId(deleteDocumentByIndexAndIdDTO);
    }

    // 批量删除索引中的文档
    @DeleteMapping("/deleteDocument")
    public RestResult<String> deleteDocument(@RequestBody DeleteDocumentDTO deleteDocumentDTO) throws IOException {
        return elasticSearchService.deleteDocument(deleteDocumentDTO);
    }

    // 查询某个索引下所有的文档
    @GetMapping("queryAllDocumentInAnIndex")
    public RestResult<?> queryAllDocumentInAnIndex(String index) throws ClassNotFoundException, IOException {
        return elasticSearchService.queryAllDocumentInAnIndex(index);
    }

    // 查询当前es中的所有索引（也能拿运行状态 大小等信息）
    @GetMapping("/getAllIndexes")
    public RestResult<?> getAllIndexes() throws IOException {
        return elasticSearchService.getAllIndexes();
    }

    //  查看某个索引下有多少个文档
    @GetMapping("queryDocumentCount")
    public RestResult<Long> getDocumentCount(String index) throws IOException {
        return elasticSearchService.getDocumentCount(index);
    }

    // 查看某个索引下的某1个文档（根据id查询）
    @GetMapping("queryDocumentById")
    public RestResult<User> queryDocumentById(QueryDocumentByIdDTO queryDocumentByIdDTO) throws IOException {
        return elasticSearchService.queryDocumentById(queryDocumentByIdDTO);
    }

    // 查看某个索引下的满足条件的文档（根据ids查询）  terms用法
    @GetMapping("queryDocumentByIds")
    public RestResult<List<?>> queryDocumentByIds(QueryDocumentByIdsDTO queryDocumentByIdsDTO) throws IOException {
        return elasticSearchService.queryDocumentByIds(queryDocumentByIdsDTO);
    }

    //  分页查询查询索引下所有的文档
    @GetMapping("/queryDocumentMatchAll")
    public RestResult<?> queryDocumentMatchAll(QueryDocumentByNameDTO queryDocumentByNameDTO) throws IOException, ClassNotFoundException {
        return elasticSearchService.queryDocumentMatchAll(queryDocumentByNameDTO);
    }

    // 模糊查询   还是以name为例  es的模糊查询和mysql的模糊查询不是一个意思   es中的模糊查询 引入了一个将编辑数的东西
    // 如果搜索的文本在指定的编辑数之内能匹配上   也回返回来  也就是传说中的自动纠错功能
    @GetMapping("queryDocumentFuzzy")
    public RestResult<?> getDocumentLike(String index, String name) throws IOException, ClassNotFoundException {
        return elasticSearchService.getDocumentLike(index, name);

    }

    // 与mysql中的模糊查询相似的是  match   同样以name字段为例
    @GetMapping("queryDocumentMatch")
    public RestResult<?> getDocumentMatch(String index, String name) throws IOException, ClassNotFoundException {
        return elasticSearchService.getDocumentMatch(index, name);
    }

    // term的查询方式
    @GetMapping("queryDocumentTerm")
    public RestResult<?> queryDocumentTerm(String index, String name) throws IOException, ClassNotFoundException {
        return elasticSearchService.queryDocumentTerm(index, name);
    }

    // 用于跨字段的查询
    @GetMapping("queryDocumentMultiMatch")
    public RestResult<?> queryDocumentMultiMatch(QueryDocumentMultiMatchDTO queryDocumentMultiMatchDTO) throws IOException, ClassNotFoundException {
        return elasticSearchService.queryDocumentMultiMatch(queryDocumentMultiMatchDTO);
    }

    // es查询  短语查询   es的match查询会分词  使用match_phrase 查询短语  如查询name=张三   就只能查出含有张三的  张某三不行
    @GetMapping("queryDocumentMatchPhrase")
    public RestResult<?> queryDocumentMatchPhrase(QueryDocumentMultiMatchDTO queryDocumentMultiMatchDTO) throws IOException, ClassNotFoundException {
        return elasticSearchService.queryDocumentMatchPhrase(queryDocumentMultiMatchDTO);
    }

    // es查询之  or -->should     这里使用should查询  比如   查询name 含有  张三   或者name含有李四的人
    @GetMapping("queryDocumentShould")
    public RestResult<?> queryDocumentShould(QueryDocumentShouldDTO queryDocumentMultiMatchDTO) throws IOException, ClassNotFoundException {
        return elasticSearchService.queryDocumentShould(queryDocumentMultiMatchDTO);
    }

    // es查询之  must     这里使用must查询  比如   查询name 含有张三 并且 name含有李四的人
    @GetMapping("queryDocumentMust")
    public RestResult<?> queryDocumentMust(QueryDocumentShouldDTO queryDocumentMultiMatchDTO) throws IOException, ClassNotFoundException {
        return elasticSearchService.queryDocumentMust(queryDocumentMultiMatchDTO);
    }

    // 实战查询
    // 在kibana中运行下面的语句  向es中添加数据  方便我们测试

    //     PUT oder/_doc/1
    //     {"id": 1, "studentNo": "TH-CHEM-2016-C001", "name": "Jonh Smith", "major":"Chemistry", "gpa": 4.8, "yearOfBorn": 2000, "classOf": 2016, "interest": "soccer, basketball, badminton, chess","productID":"XHDK-A-1293-#fJ3", "price",10}
    //       PUT oder/_doc/2"productID":    , "price",,
    //     {"id": 2, "studentNo": "TH-PHY-2018-C001", "name": "Isaac Newton", "major":"Physics", "gpa": 3.6, "yearOfBorn": 2001, "classOf": 2018, "interest": "novel, soccer, cooking","productID":"KDKE-B-9947-#kL5", "price",20}
    //       PUT oder/_doc/3"productID":    , "price",,
    //     {"id": 3, "studentNo": "BU-POLI-2016-C001", "name": "John Kennedy", "major":"Politics", "gpa": 4.2, "yearOfBorn": 2000, "classOf": 2016, "interest": "talking, dating, boxing, shooting, chess","productID":"JODL-X-1937-#pV7","price", 30}
    //       PUT oder/_doc/4"productID":    , "price",,
    //     {"id": 4, "studentNo": "BU-POLI-2015-C001", "name": "John Kerry", "major":"Politics", "gpa": 4.1, "yearOfBorn": 1999, "classOf": 2015, "interest": "money, basketball","productID":"QQPX-R-3956-#aD8","price", 30}
    //       PUT oder/_doc/5"productID":    , "price",,
    //     {"id": 5, "studentNo": "BU-ARTS-2016-C002", "name": "Da Vinci", "major":"Arts", "gpa": 4.8, "yearOfBorn": 1995, "classOf": 2016, "interest": "drawing, music, wine","productID":"QQPX-R-3956-#aD8","price", 30}


    // 1. # 查询具有"XHDK-A-1293-#fJ3"特定商品id的信息。
    //# 使用term来查找只能是long...,date, kerword类型的数据
    @GetMapping("test1")
    public RestResult<?> test1() throws IOException {
        return elasticSearchService.test1();
    }

    //  查询价格在20-40之间的商品信息
    @GetMapping("test2")
    public RestResult<?> test2() throws IOException {
        return elasticSearchService.test2();
    }

    // 查询商品价格为30或者"productID"为"XHDK-A-1293-#fJ3"的商品信息，但是商品的"productID"不能为"QQPX-R-3956-#aD8"
    @GetMapping("test3")
    public RestResult<?> test3() throws IOException {
        return elasticSearchService.test3();
    }

    // 查询productID 为"KDKE-B-9947-#kL5"的商品信息或者 productID为"JODL-X-1937-#pV7" 并且同时 price为 30的商品信息
    @GetMapping("test4")
    public RestResult<?> test4() throws IOException {
        return elasticSearchService.test4();
    }

    // 聚合函数的使用  比如对上述数据中的price进行求平均值
    @GetMapping("test5")
    public RestResult<?> test5() throws IOException {
        return elasticSearchService.test5();
    }

    // 聚合函数的使用  比如对上述数据中的price进行求和
    @GetMapping("test6")
    public RestResult<?> test6() throws IOException {
        return elasticSearchService.test6();
    }

    // 聚合函数的使用  比如对上述数据中的price进行求最大值
    @GetMapping("test7")
    public RestResult<?> test7() throws IOException {
        return elasticSearchService.test7();
    }

    // 聚合函数的使用  比如对上述数据中的price进行求最小值
    @GetMapping("test8")
    public RestResult<?> test8() throws IOException {
        return elasticSearchService.test8();
    }

    // 对查询结果按price进行排序
    @GetMapping("test9")
    public RestResult<?> test9() throws IOException {
        return elasticSearchService.test9();
    }


}
