package com.example.elastic_search.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author YangLi
 * @Date 2023/10/30 13:59
 * @注释
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestResult<T> {

    private Integer status;

    private String message;

    private T data;

    public static RestResult<String> failure() {
        RestResult<String> restResult = new RestResult<>();
        restResult.setStatus(0);
        restResult.setMessage("操作失败");
        return restResult;
    }

    public static RestResult<String> failure(String message) {
        RestResult<String> restResult = new RestResult<>();
        restResult.setStatus(0);
        restResult.setMessage(message);
        return restResult;
    }

    public static RestResult<String> success() {
        RestResult<String> restResult = new RestResult<>();
        restResult.setStatus(1);
        restResult.setMessage("操作成功");
        return restResult;
    }

    public static <T> RestResult<T> success(T data) {
        RestResult<T> restResult = new RestResult<>();
        restResult.setStatus(1);
        restResult.setData(data);
        restResult.setMessage("操作成功");
        return restResult;
    }
}
