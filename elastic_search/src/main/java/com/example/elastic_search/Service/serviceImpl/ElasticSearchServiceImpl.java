package com.example.elastic_search.Service.serviceImpl;

import cn.hutool.core.collection.CollUtil;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregate;
import co.elastic.clients.elasticsearch._types.aggregations.AvgAggregate;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch.cat.IndicesResponse;
import co.elastic.clients.elasticsearch.core.*;
import co.elastic.clients.elasticsearch.core.bulk.BulkOperation;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.json.JsonData;
import com.alibaba.fastjson.JSONObject;
import com.example.elastic_search.Service.ElasticSearchService;
import com.example.elastic_search.Utils.EsUtil;
import com.example.elastic_search.constants.ElasticSearchExceptionConstants;
import com.example.elastic_search.domain.entity.Oder;
import com.example.elastic_search.domain.requestDTO.*;
import com.example.elastic_search.domain.entity.User;
import com.example.elastic_search.result.RestResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Author YangLi
 * @Date 2023/10/26 10:08
 * @注释
 */
@Service
public class ElasticSearchServiceImpl implements ElasticSearchService {

    private static final String classPath = "com.example.elastic_search.domain.entity.%s";

    @Resource
    public ElasticsearchClient elasticsearchClient;

    @Override
    public RestResult<String> createIndex(String index) {
        try {
            return RestResult.success(String.valueOf(elasticsearchClient.indices().create(c -> c.index(index)).acknowledged()));
        } catch (Exception e) {
            e.printStackTrace();
            return RestResult.failure();
        }
    }

    @Override
    public RestResult<String> deleteIndex(String index) {
        try {
            return RestResult.success(String.valueOf(elasticsearchClient.indices().delete(c -> c.index(index)).acknowledged()));
        } catch (Exception e) {
            e.printStackTrace();
            return RestResult.failure();
        }
    }

    @Override
    public RestResult<String> createDocument(User user) {
        try {
            return RestResult.success(elasticsearchClient.create(item -> item.index("user")
                    .id(user.getId())
                    .type("_doc")
                    .document(user)).result().jsonValue());
        } catch (Exception e) {
            e.printStackTrace();
            if (e.getMessage().contains("document already exists")) {
                return RestResult.failure(ElasticSearchExceptionConstants.DOCUMENT_ALREADY_EXISTS);
            } else {
                return RestResult.failure();
            }
        }
    }

    @Override
    public RestResult<String> addDocument(AddUserDTO addUserDTO) throws IOException {
        List<BulkOperation> bulkOperations = new ArrayList<>();
        // 搞点假数据
        ArrayList<User> list = new ArrayList<>();
        for (int i = 4; i < 1200; i++) {
            User user = new User(String.valueOf(i), "alice" + i, i);
            list.add(user);
        }

        // 这一步是将users中的user用文档的形式放入上面这个list中                                            // 这里的id是文档自带的id  如果不指定  则是由es自动生成
        list.forEach(item -> bulkOperations.add(BulkOperation.of(x -> x.index(c -> c.id(item.getId())
                .document(item)))));
        BulkResponse bulk = elasticsearchClient.bulk(x -> x.index(addUserDTO.getIndex())
                .operations(bulkOperations));
        if (bulk.errors())
            return RestResult.failure("添加失败");
        return RestResult.success();
    }

    @Override
    public RestResult<String> updateDocument(UpdateDocumentDTO updateDocumentDTO) {
        try {
            Class<?> clazz = Class.forName(String.format(classPath, updateDocumentDTO.getIndex().replaceFirst(updateDocumentDTO.getIndex().substring(0, 1), updateDocumentDTO.getIndex().substring(0, 1).toUpperCase())));
            // 将前端传过来的map转成 与Index同名的类   比如  前端传递index = user和一个hashMap  那么就将这个hashMap转成 User 类
            //                                         前端传递index = Alice和一个hashMap  那么就将这个hashMap转成 Alice 类...  并返回id的值
            Object object = JSONObject.parseObject(JSONObject.toJSONString(updateDocumentDTO.getHashMap()), clazz);
            Field[] declaredFields = object.getClass().getDeclaredFields();
            for (Field declaredField : declaredFields) {
                if (declaredField.getName().equals("id")) {
                    declaredField.setAccessible(true);
                    updateDocumentDTO.setId(String.valueOf(declaredField.get(object)));
                    break;
                }
            }
            return RestResult.success(elasticsearchClient.update(x ->
                    x.index(updateDocumentDTO.getIndex())
                            .id(updateDocumentDTO.getId())
                            // type 默认也是_doc
                            .type("_doc")
                            .doc(object), clazz)
                    .result().jsonValue());
        } catch (Exception e) {
            e.printStackTrace();
            return RestResult.failure(e.getMessage());
        }
    }

    @Override
    public RestResult<String> deleteDocument(DeleteDocumentDTO deleteDocumentDTO) throws IOException {
        ArrayList<BulkOperation> bulkOperations = new ArrayList<>();
        //                                                                                                 这个id是索引中自带的id  不是document中的id
        deleteDocumentDTO.getIds().forEach(item -> bulkOperations.add(BulkOperation.of(c -> c.delete(x -> x.id(item)))));

        BulkResponse bulk = elasticsearchClient.bulk(x -> x.index(deleteDocumentDTO.getIndex()).operations(bulkOperations));
        if (bulk.errors())
            return RestResult.failure("删除失败");
        return RestResult.success();
    }

    @Override
    public RestResult<String> deleteDocumentByIndexAndId(DeleteDocumentByIndexAndIdDTO deleteDocumentByIndexAndIdDTO) {
        try {
            return RestResult.success(elasticsearchClient.delete(x -> x.index(deleteDocumentByIndexAndIdDTO.getIndex()).id(deleteDocumentByIndexAndIdDTO.getId())).result().jsonValue());
        } catch (Exception e) {
            e.printStackTrace();
            return RestResult.failure("删除失败");
        }
    }

    @Override
    public RestResult<?> queryAllDocumentInAnIndex(String index) throws ClassNotFoundException, IOException {
        // 指定index  SearchResponse<>  里面可以放指定的类  我这里为了适配所有的类就用了基类
        // get 和  search 的区别     get需要指定id   search不能指定id  这里的id 是es自动生成的id
        SearchResponse<?> searchResponse = elasticsearchClient.search(item -> item.index(index), Class.forName(String.format(classPath, index.replaceFirst(index.substring(0, 1), index.substring(0, 1).toUpperCase()))));
        return RestResult.success(EsUtil.getObjectsFromHitsToList(searchResponse));
    }

    @Override
    public RestResult<?> getAllIndexes() throws IOException {
        IndicesResponse indices = elasticsearchClient.cat().indices();
        ArrayList<String> list = new ArrayList<>();
        indices.valueBody().forEach(item ->
                list.add(item.index()));
        return RestResult.success(list);
    }

    @Override
    public RestResult<Long> getDocumentCount(String index) throws IOException {
        long count = elasticsearchClient.count(a -> a.index(index)).count();
        return RestResult.success(count);
    }

    @Override
    public RestResult<User> queryDocumentById(QueryDocumentByIdDTO queryDocumentByIdDTO) throws IOException {
        // get 和  search 的区别     get需要指定id   search不能指定id  这里的id 是es自动生成的id
        GetResponse<User> userGetResponse = elasticsearchClient.get(a -> a.index(queryDocumentByIdDTO.getIndex()).id(queryDocumentByIdDTO.getId()), User.class);
        return RestResult.success(userGetResponse.source());
    }

    @Override
    public RestResult<List<?>> queryDocumentByIds(QueryDocumentByIdsDTO queryDocumentByIdsDTO) throws IOException {
        int batch = 1000;
        //  这个list中转了多个Query   每个Query中装了 1000个查询参数
        ArrayList<Query> list = new ArrayList<>();
        //1000个数据切割一次   每1000个数据构建一个Query
        // CollUtil.split(queryDocumentByIdsDTO.getIds(), batch)   这是将list分成多个list 然后放在一个list中
        for (List<String> value1000 : CollUtil.split(queryDocumentByIdsDTO.getIds(), batch)) {
            Query query = Query.of(qb -> qb
                    .terms(termsBuilder -> termsBuilder
                            .field("id")
                            .terms(ts -> ts.value(
                                    // 这里需要FiledValue的值
                                    value1000.stream()
                                            // FieldValue::of   意思是将这个list中的元素全部转成FieldValue类型
                                            .map(FieldValue::of)
                                            // 下面是将转换后的元素收集成一个集合
                                            .collect(Collectors.toList()))
                            )
                    )
            );
            list.add(query);
        }

        //拼成一个查询语句
        Query query = Query.of(qb -> qb
                .bool(bool -> bool
                        .should(list)
                )
        );

        SearchResponse<Object> search1 = elasticsearchClient.search(SearchRequest.of(search -> search
                        .query(query)
                        .index(queryDocumentByIdsDTO.getIndex())
                        .size(5000)
                ),
                //这里使用es 默认的json解析 不太好用 不太灵活
                Object.class);
        List<Hit<Object>> hits = search1.hits().hits();
        ArrayList<Object> list1 = new ArrayList<>();
        hits.forEach(item -> list1.add(item.source()));

        return RestResult.success(list1);
    }

    @Override
    public RestResult<?> queryDocumentMatchAll(QueryDocumentByNameDTO queryDocumentByNameDTO) throws IOException, ClassNotFoundException {
        SearchResponse<?> search = elasticsearchClient.search(a -> a
                        .index(queryDocumentByNameDTO.getIndex())
                        .query(q -> q
                                .matchAll(match -> match))
                        // 分页查询
                        .from((queryDocumentByNameDTO.getCurrentPage() - 1) * queryDocumentByNameDTO.getPageSize())
                        .size(queryDocumentByNameDTO.getPageSize())
                , Class.forName(String.format(classPath, queryDocumentByNameDTO.getIndex().replaceFirst(queryDocumentByNameDTO.getIndex().substring(0, 1), queryDocumentByNameDTO.getIndex().substring(0, 1).toUpperCase()))));
        ArrayList<Object> list = EsUtil.getObjectsFromHitsToList(search);
        return RestResult.success(list);
    }

    @Override
    public RestResult<?> getDocumentLike(String index, String name) throws ClassNotFoundException, IOException {
        SearchResponse<?> search = elasticsearchClient.search(a -> a
                        .index(index)
                        .query(q -> q
                                .fuzzy(f -> f
                                        .field("name")
                                        .value(name)
                                        .fuzziness("1") // 允许偏差的个数
                                        .prefixLength(1)  // 前几个字符必须满足
                                )
                        )
                        .source(s -> s
                                .filter(f -> f
                                        .includes("name", "age")
                                )
                        )
                , Class.forName(String.format(classPath, index.replaceFirst(index.substring(0, 1), index.substring(0, 1).toUpperCase()))));
        return RestResult.success(EsUtil.getObjectsFromHitsToList(search));
    }

    @Override
    public RestResult<?> getDocumentMatch(String index, String name) throws ClassNotFoundException, IOException {
        SearchResponse<?> search = elasticsearchClient.search(a -> a
                        .index(index)
                        .query(q -> q
                                .match(m -> m
                                        .field("name")
                                        .query(name)
                                )
                        )

                        //  对结果字段进行过滤   includes   要返回的字段    excludes   要筛掉的字段
                        .source(s -> s
                                .filter(f -> f
                                        .includes("age", "name", "id")))
                , Class.forName(String.format(classPath, index.replaceFirst(index.substring(0, 1), index.substring(0, 1).toUpperCase()))));
        return RestResult.success(EsUtil.getObjectsFromHitsToList(search));
    }

    @Override
    public RestResult<?> queryDocumentTerm(String index, String name) throws ClassNotFoundException, IOException {
        SearchResponse<?> search = elasticsearchClient.search(a -> a
                        .index("")
                        .query(q -> q
                                .term(t -> t
                                        .field("name")
                                        .value(name)))
                , Class.forName(String.format(classPath, index.replaceFirst(index.substring(0, 1), index.substring(0, 1).toUpperCase()))));
        return RestResult.success(EsUtil.getObjectsFromHitsToList(search));
    }

    @Override
    public RestResult<?> queryDocumentMultiMatch(QueryDocumentMultiMatchDTO queryDocumentMultiMatchDTO) throws ClassNotFoundException, IOException {

        //  第一种查询方式   一口气将所有匹配条件全
        SearchResponse<?> search = elasticsearchClient.search(a -> a
                        .index(queryDocumentMultiMatchDTO.getIndex())
                        .query(q -> q
                                .multiMatch(m -> m
                                        .query(queryDocumentMultiMatchDTO.getValue())
                                        //  查询这个value  在id和age 字段同时满足的情况  使用multi_match
                                        .fields("id", "age")
                                )
                        )
                , Class.forName(String.format(classPath, queryDocumentMultiMatchDTO.getIndex().replaceFirst(queryDocumentMultiMatchDTO.getIndex().substring(0, 1), queryDocumentMultiMatchDTO.getIndex().substring(0, 1).toUpperCase()))));
        return RestResult.success(EsUtil.getObjectsFromHitsToList(search));
    }

    @Override
    public RestResult<?> queryDocumentMatchPhrase(QueryDocumentMultiMatchDTO queryDocumentMultiMatchDTO) throws ClassNotFoundException, IOException {
        SearchResponse<?> search = elasticsearchClient.search(a -> a
                        .index(queryDocumentMultiMatchDTO.getIndex())
                        .query(q -> q
                                .matchPhrase(m -> m
                                        .field("name")
                                        .query(queryDocumentMultiMatchDTO.getValue())
                                )
                        )
                , Class.forName(String.format(classPath, queryDocumentMultiMatchDTO.getIndex().replaceFirst(queryDocumentMultiMatchDTO.getIndex().substring(0, 1), queryDocumentMultiMatchDTO.getIndex().substring(0, 1).toUpperCase()))));
        return RestResult.success(EsUtil.getObjectsFromHitsToList(search));
    }

    @Override
    public RestResult<?> queryDocumentShould(QueryDocumentShouldDTO queryDocumentMultiMatchDTO) throws ClassNotFoundException, IOException {
        //  查询名字中带有张三或者李四的
        SearchResponse<?> search = elasticsearchClient.search(a -> a
                        .index(queryDocumentMultiMatchDTO.getIndex())
                        .query(q -> q
                                .bool(b -> b
                                        .should(s -> s
                                                .match(m -> m
                                                        .field("name")
                                                        .query(queryDocumentMultiMatchDTO.getValues().get(0))
                                                )
                                        )
                                        .should(s -> s
                                                .match(m -> m
                                                        .field("name")
                                                        .query(queryDocumentMultiMatchDTO.getValues().get(1))
                                                )
                                        )
                                )
                        )
                , Class.forName(String.format(classPath, queryDocumentMultiMatchDTO.getIndex().replaceFirst(queryDocumentMultiMatchDTO.getIndex().substring(0, 1), queryDocumentMultiMatchDTO.getIndex().substring(0, 1).toUpperCase()))));
        ArrayList<Query> list = new ArrayList<>();
        // 创建一个list的query
        queryDocumentMultiMatchDTO.getValues().forEach(item -> {
            Query name = Query.of(mustBuild -> mustBuild
                    .match(match -> match
                            .field("name")
                            .query(item)
                    )
            );
            list.add(name);
        });
        // 将创建的list整成Query
        Query query = Query.of(q -> q.bool(b -> b.must(list)));

        SearchResponse<?> search1 = elasticsearchClient.search(a -> a
                        .query(query)
                        .index(queryDocumentMultiMatchDTO.getIndex())
                        .size(10)
                , Class.forName(String.format(classPath, queryDocumentMultiMatchDTO.getIndex().replaceFirst(queryDocumentMultiMatchDTO.getIndex().substring(0, 1), queryDocumentMultiMatchDTO.getIndex().substring(0, 1).toUpperCase()))));


        // 查询名字为张三或者李四的
        SearchResponse<?> name = elasticsearchClient.search(a -> a
                        .index(queryDocumentMultiMatchDTO.getIndex())
                        .query(q -> q
                                .terms(t -> t
                                        .field("name.keyword")
                                        .terms(te -> te
                                                .value(
                                                        queryDocumentMultiMatchDTO.getValues()
                                                                .stream()
                                                                .map(FieldValue::of)
                                                                .collect(Collectors.toList())
                                                )
                                        )
                                )
                        )
                , Class.forName(String.format(classPath, queryDocumentMultiMatchDTO.getIndex().replaceFirst(queryDocumentMultiMatchDTO.getIndex().substring(0, 1), queryDocumentMultiMatchDTO.getIndex().substring(0, 1).toUpperCase()))));
        return RestResult.success(EsUtil.getObjectsFromHitsToList(name));
    }

    @Override
    public RestResult<?> queryDocumentMust(QueryDocumentShouldDTO queryDocumentMultiMatchDTO) throws ClassNotFoundException, IOException {
        // 查询名字中含有张三李四
        elasticsearchClient.search(a -> a
                        .index(queryDocumentMultiMatchDTO.getIndex())
                        .query(q -> q
                                .bool(b -> b
                                        .must(m -> m
                                                .match(match -> match
                                                        .field("name")
                                                        .query(queryDocumentMultiMatchDTO.getValues().get(0))
                                                )
                                        )
                                        .must(m -> m
                                                .match(match -> match
                                                        .field("name")
                                                        .query(queryDocumentMultiMatchDTO.getValues().get(1))
                                                )
                                        )
                                )
                        )
                , Class.forName(String.format(classPath, queryDocumentMultiMatchDTO.getIndex().replaceFirst(queryDocumentMultiMatchDTO.getIndex().substring(0, 1), queryDocumentMultiMatchDTO.getIndex().substring(0, 1).toUpperCase()))));
        return null;
    }

    /**
     * // 1. # 查询具有"XHDK-A-1293-#fJ3"特定商品id的信息。
     * //# 使用term来查找只能是long...,date, kerword类型的数据
     *
     * @return .
     * @throws IOException .
     */
    @Override
    public RestResult<?> test1() throws IOException {
        SearchResponse<?> search = elasticsearchClient.search(a -> a.index("oder").query(q -> q.term(t -> t.field("productId.keyword").value("XHDK-A-1293-#fJ3"))), Oder.class);
        return RestResult.success(EsUtil.getObjectsFromHitsToList(search));
    }

    /**
     * 查询价格在20-40之间的商品信息
     *
     * @return .
     * @throws IOException .
     */
    @Override
    public RestResult<?> test2() throws IOException {
        SearchResponse<Oder> search = elasticsearchClient.search(a -> a.index("oder")
                        .query(q -> q
                                .range(r -> r
                                        .field("price")
                                        .lte(JsonData.of(40))
                                        .gte(JsonData.of(20))))
                , Oder.class);
        return RestResult.success(EsUtil.getObjectsFromHitsToList(search));
    }

    /**
     * // 查询商品价格为30或者"productID"为"XHDK-A-1293-#fJ3"的商品信息，但是商品的"productID"不能为"QQPX-R-3956-#aD8"
     *
     * @return .
     */
    @Override
    public RestResult<?> test3() throws IOException {
        SearchResponse<Oder> search = elasticsearchClient.search(a -> a
                        .index("oder")
                        .query(q -> q
                                .bool(b -> b
                                        .should(s -> s
                                                .match(m -> m
                                                        .field("price")
                                                        .query(30)
                                                )
                                        )
                                        .should(s -> s
                                                .match(m -> m
                                                        .field("productId")
                                                        .query("XHDK-A-1293-#fJ3")
                                                )
                                        )
                                        .mustNot(mn -> mn
                                                .match(ma -> ma
                                                        .field("productId")
                                                        .query("QQPX-R-3956-#aD8")
                                                )
                                        )
                                )
                        )
                , Oder.class);
        return RestResult.success(EsUtil.getObjectsFromHitsToList(search));
    }

    /**
     * 查询productID 为"KDKE-B-9947-#kL5"的商品信息或者 productID为"JODL-X-1937-#pV7" 并且同时 price为 30的商品信息
     *
     * @return .
     */
    @Override
    public RestResult<?> test4() throws IOException {
        SearchResponse<Oder> search = elasticsearchClient.search(a -> a
                        .index("oder")
                        .query(q -> q
                                .bool(b -> b
                                        .must(m -> m
                                                .bool(bool -> bool
                                                        .should(s -> s
                                                                .match(ma -> ma
                                                                        .field("productId")
                                                                        .query("KDKE-B-9947-#kL5")
                                                                )
                                                        )
                                                        .should(s -> s
                                                                .match(ma -> ma
                                                                        .field("productId")
                                                                        .query("JODL-X-1937-#pV7")
                                                                )
                                                        )
                                                )
                                        )
                                        .must(m -> m
                                                .match(ma -> ma
                                                        .field("price")
                                                        .query(30)
                                                )
                                        )
                                )
                        )
                , Oder.class);

        return RestResult.success(EsUtil.getObjectsFromHitsToList(search));
    }

    /**
     * 对price求平均值
     *
     * @return 。
     */
    @Override
    public RestResult<?> test5() throws IOException {
        SearchResponse<Oder> oder = elasticsearchClient.search(a -> a
                        .index("oder")
                        // 表示不查询文档本身数据
                        .size(0)
                        .aggregations("avg_price", agg -> agg
                                .avg(avg -> avg
                                        .field("price")
                                        //  这里的missing表示  将price为null的地方换成0
                                        .missing(0)
                                )
                        )
                , Oder.class);
        return RestResult.success(oder.aggregations().get("avg_price").avg().value());
    }

    /**
     * 对price求和
     *
     * @return 。
     */
    @Override
    public RestResult<?> test6() throws IOException {
        SearchResponse<Oder> oder = elasticsearchClient.search(a -> a
                        .index("oder")
                        .size(0)
                        .aggregations("sum_price", sum -> sum
                                .sum(s -> s
                                        .field("price")
                                        .missing(0)
                                )
                        )
                , Oder.class);
        return RestResult.success(oder.aggregations().get("sum_price").sum().value());
    }

    /**
     * 对price求大值
     *
     * @return 。
     */
    @Override
    public RestResult<?> test7() throws IOException {
        SearchResponse<Oder> oder = elasticsearchClient.search(a -> a
                        .index("oder")
                        .size(0)
                        .aggregations("max_price", max -> max
                                .max(s -> s
                                        .field("price") //  最大值最小值 不需要使用missing将null换成0
                                )
                        )
                , Oder.class);
        return RestResult.success(oder.aggregations().get("max_price").max().value());
    }

    /**
     * 对price求最小值
     *
     * @return 。
     */
    @Override
    public RestResult<?> test8() throws IOException {
        SearchResponse<Oder> oder = elasticsearchClient.search(a -> a
                        .index("oder")
                        .size(0)
                        .query(q -> q
                                .terms(t -> t
                                        .field("id")
                                        .terms(ts -> ts
                                                .value(Stream.of(4, 5, 6)
                                                        .map(FieldValue::of)
                                                        .collect(Collectors.toList())
                                                )
                                        )
                                )
                        )
                        //  对查询结果进行求最小值
                        .aggregations("min_price", min -> min
                                .min(s -> s
                                        .field("price")  //  最大值最小值 不需要使用missing将null换成0
                                )
                        )
                , Oder.class);
        return RestResult.success(oder.aggregations().get("min_price").min().value());
    }

    /**
     * 对price进行排序
     *
     * @return 。
     */
    @Override
    public RestResult<?> test9() throws IOException {
        SearchResponse<Oder> oder = elasticsearchClient.search(a -> a
                        .index("oder")
                        .from(0)
                        .size(10)
                        .query(q -> q
                                .matchAll(match -> match)
                        )
                        //  按某个字段进行排序
                        .sort(s -> s
                                .field(f -> f
                                        .field("price")
                                        .order(SortOrder.Desc)
                                )
                        )

                , Oder.class);
        return RestResult.success(EsUtil.getObjectsFromHitsToList(oder));
    }
}
