package com.example.elastic_search.Service;

import com.example.elastic_search.domain.requestDTO.*;
import com.example.elastic_search.domain.entity.User;
import com.example.elastic_search.result.RestResult;

import java.io.IOException;
import java.util.List;

/**
 * @Author YangLi
 * @Date 2023/10/26 10:07
 * @注释
 */
public interface ElasticSearchService {

    RestResult<String> createIndex(String index) throws IOException;

    RestResult<String> deleteIndex(String index) throws IOException;

    RestResult<String> deleteDocumentByIndexAndId(DeleteDocumentByIndexAndIdDTO deleteDocumentByIndexAndIdDTO);

    RestResult<String> createDocument(User user);

    RestResult<?> queryAllDocumentInAnIndex(String index) throws ClassNotFoundException, IOException;

    RestResult<String> updateDocument(UpdateDocumentDTO updateDocumentDTO) throws IOException;

    RestResult<String> addDocument(AddUserDTO addUserDTO) throws IOException;

    RestResult<String> deleteDocument(DeleteDocumentDTO deleteDocumentDTO) throws IOException;

    RestResult<?> getAllIndexes() throws IOException;

    RestResult<Long> getDocumentCount(String index) throws IOException;

    RestResult<User> queryDocumentById(QueryDocumentByIdDTO queryDocumentByIdDTO) throws IOException;

    RestResult<List<?>> queryDocumentByIds(QueryDocumentByIdsDTO queryDocumentByIdsDTO) throws IOException;

    RestResult<?> queryDocumentMatchAll(QueryDocumentByNameDTO queryDocumentByNameDTO) throws IOException, ClassNotFoundException;

    RestResult<?> getDocumentLike(String index, String name) throws ClassNotFoundException, IOException;

    RestResult<?> getDocumentMatch(String index, String name) throws ClassNotFoundException, IOException;

    RestResult<?> queryDocumentTerm(String index, String name) throws ClassNotFoundException, IOException;

    RestResult<?> queryDocumentMultiMatch(QueryDocumentMultiMatchDTO queryDocumentMultiMatchDTO) throws ClassNotFoundException, IOException;

    RestResult<?> queryDocumentMatchPhrase(QueryDocumentMultiMatchDTO queryDocumentMultiMatchDTO) throws ClassNotFoundException, IOException;

    RestResult<?> queryDocumentShould(QueryDocumentShouldDTO queryDocumentMultiMatchDTO) throws ClassNotFoundException, IOException;

    RestResult<?> queryDocumentMust(QueryDocumentShouldDTO queryDocumentMultiMatchDTO) throws ClassNotFoundException, IOException;

    RestResult<?> test1() throws IOException;

    RestResult<?> test2() throws IOException;

    RestResult<?> test3() throws IOException;

    RestResult<?> test4() throws IOException;

    RestResult<?> test5() throws IOException;

    RestResult<?> test6() throws IOException;

    RestResult<?> test7() throws IOException;

    RestResult<?> test8() throws IOException;

    RestResult<?> test9() throws IOException;
}
