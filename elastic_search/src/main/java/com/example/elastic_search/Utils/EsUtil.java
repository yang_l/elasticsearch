package com.example.elastic_search.Utils;

import co.elastic.clients.elasticsearch.core.SearchResponse;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * @Author YangLi
 * @Date 2023/11/10 11:36
 * @注释
 */
public class EsUtil {

    public static ArrayList<Object> getObjectsFromHitsToList(SearchResponse<?> search) {
        ArrayList<Object> list = new ArrayList<>();
        search.hits().hits().forEach(item -> {
            assert item.source() != null;
            Field[] declaredFields = item.source().getClass().getDeclaredFields();
            for (Field declaredField : declaredFields) {
                if (declaredField.getName().equals("id")) {
                    try {
                        declaredField.setAccessible(true);
                        if (declaredField.get(item.source()) == null) {
                            declaredField.set(item.source(), item.id());
                            break;
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            list.add(item.source());
        });
        return list;
    }
}
