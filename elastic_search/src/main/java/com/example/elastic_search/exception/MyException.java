package com.example.elastic_search.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author YangLi
 * @Date 2023/11/6 15:34
 * @注释
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MyException extends RuntimeException {

    private String msg;
    private Integer code;

    public MyException(String msg, Throwable cause) {
        super(msg, cause);
        this.msg = msg;
    }

    public MyException(String msg) {
        this.code = 500;
        this.msg = msg;
    }
}
