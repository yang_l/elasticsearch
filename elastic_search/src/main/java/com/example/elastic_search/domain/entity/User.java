package com.example.elastic_search.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author YangLi
 * @Date 2023/10/26 10:03
 * @注释
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Field(type = FieldType.Long, name = "id")
    private String id;

    @Field(name = "name", type = FieldType.Text)
    private String name;

    @Field(name = "age", type = FieldType.Long)
    private Integer age;

    @Field(name = "birthday", type = FieldType.Date)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    public User(String id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
}
