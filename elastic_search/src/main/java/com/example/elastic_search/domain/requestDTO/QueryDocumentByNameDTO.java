package com.example.elastic_search.domain.requestDTO;

import lombok.Data;

/**
 * @Author YangLi
 * @Date 2023/11/10 10:32
 * @注释
 */
@Data
public class QueryDocumentByNameDTO {

    private String index;

    private Integer currentPage;

    private Integer pageSize;
}
