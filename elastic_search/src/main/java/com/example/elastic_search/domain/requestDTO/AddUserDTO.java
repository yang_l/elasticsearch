package com.example.elastic_search.domain.requestDTO;

import com.example.elastic_search.domain.entity.User;
import lombok.Data;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2023/11/3 16:04
 * @注释
 */
@Data
public class AddUserDTO {

    private List<User> users;

    private String index;

}
