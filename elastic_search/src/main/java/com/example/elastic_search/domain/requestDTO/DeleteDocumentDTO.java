package com.example.elastic_search.domain.requestDTO;

import lombok.Data;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2023/11/3 17:00
 * @注释
 */
@Data
public class DeleteDocumentDTO {

    private String index;

    private List<String> ids;
}
