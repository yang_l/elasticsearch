package com.example.elastic_search.domain.requestDTO;

import lombok.Data;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2023/11/10 18:25
 * @注释
 */
@Data
public class QueryDocumentShouldDTO {

    private String index;

    private List<String> values;
}
