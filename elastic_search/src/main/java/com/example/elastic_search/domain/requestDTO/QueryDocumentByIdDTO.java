package com.example.elastic_search.domain.requestDTO;

import lombok.Data;

/**
 * @Author YangLi
 * @Date 2023/11/9 14:40
 * @注释
 */
@Data
public class QueryDocumentByIdDTO {

    private String index;

    private String id;
}
