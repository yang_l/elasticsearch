package com.example.elastic_search.domain.requestDTO;

import lombok.Data;

import java.util.HashMap;

/**
 * @Author YangLi
 * @Date 2023/11/3 17:26
 * @注释
 */
@Data
public class UpdateDocumentDTO {

    private String index;

    private HashMap<String, Object> hashMap;

    private String id;
}
