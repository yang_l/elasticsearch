package com.example.elastic_search.domain.entity;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

/**
 * @Author YangLi
 * @Date 2023/11/3 11:31
 * @注释
 */
@Data
public class Oder {

    @Field(type = FieldType.Long, name = "id")
    private String id;

    @Field(name = "studentNo", type = FieldType.Text)
    private String studentNo;

    @Field(name = "name", type = FieldType.Text)
    private String name;

    @Field(name = "major", type = FieldType.Text)
    private String major;

    @Field(name = "gpa", type = FieldType.Float)
    private Float gpa;

    @Field(name = "yearOfBorn", type = FieldType.Date)
    private Date yearOfBorn;

    @Field(name = "classOf", type = FieldType.Date)
    private Date classOf;

    @Field(name = "interest", type = FieldType.Text)
    private String interest;

    @Field(name = "productId", type = FieldType.Text)
    private String productId;

    @Field(name = "price", type = FieldType.Double)
    private Double price;
}
