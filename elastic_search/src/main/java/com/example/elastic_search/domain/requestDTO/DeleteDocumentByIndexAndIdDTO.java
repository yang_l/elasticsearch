package com.example.elastic_search.domain.requestDTO;

import lombok.Data;

/**
 * @Author YangLi
 * @Date 2023/11/3 17:21
 * @注释
 */
@Data
public class DeleteDocumentByIndexAndIdDTO {

    private String id;

    private String index;
}
