package com.example.elastic_search.domain.requestDTO;

import lombok.Data;

/**
 * @Author YangLi
 * @Date 2023/11/10 17:29
 * @注释
 */
@Data
public class QueryDocumentMultiMatchDTO {

    private String index;

    // 需要查询的值
    private String value;
}
