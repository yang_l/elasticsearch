package com.example.elastic_search.domain.requestDTO;

import lombok.Data;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2023/11/8 18:31
 * @注释
 */
@Data
public class QueryDocumentByIdsDTO {

    private String index;

    private List<String> ids;
}
