package com.example.elastic_search.domain.entity;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2023/11/2 16:07
 * @注释
 */
@Data
public class Alice {

    @Field(type = FieldType.Long, name = "id")
    private String id;

    @Field(name = "name", type = FieldType.Text)
    private String name;

    @Field(name = "desc", type = FieldType.Text)
    private String desc;

    @Field(name = "age", type = FieldType.Text)
    private Integer age;

    @Field(name = "tags", type = FieldType.Text)
    private List<String> tags;

}
