package com.example.elastic_search.constants;

/**
 * @Author YangLi
 * @Date 2023/10/30 11:40
 * @注释
 */
public class ElasticSearchExceptionConstants {

    public static final String DOCUMENT_ALREADY_EXISTS = "文档已存在";

    public static final String UNRECOGNIZED_FIELD = "存在无法识别的字段";

    public static final String NO_SUCH_INDEX = "索引不存在";
}
